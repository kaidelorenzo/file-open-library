#include "myio.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int compare_files(char *fname1, char *fname2);

int simple_write_test();
int simple_read_test();
int read_twice_from_file();
int write_twice_to_file();
int large_read_test();
int large_double_read_test();
int extra_large_read_test();
int large_write_test();
int extra_large_write_test();
int write_to_multiple_of_chunk_then_write();
int read_rest_of_chunk_then_read_again();
int write_past_EOF();
int read_past_EOF();
int read_much_past_EOF();

int simple_write_test_rw();
int simple_read_test_rw();
int read_twice_from_file_rw();
int write_twice_to_file_rw();
int read_then_write_from_file();
int write_then_read_from_file();
int large_read_test_rw();
int large_double_read_test_rw();
int extra_large_read_test_rw();
int large_write_test_rw();
int extra_large_write_test_rw();
int large_read_then_write();
int extra_large_read_then_extra_large_write();
int large_write_then_read();
int extra_large_write_then_extra_large_read();
int write_to_multiple_of_chunk_then_write_rw();
int read_rest_of_chunk_then_read_again_rw();
int write_to_multiple_of_chunk_then_read();
int read_rest_of_chunk_then_write();

int seekset_within_chunk_then_write();
int seekcur_within_chunk_then_write();
int seekset_within_chunk_then_read();
int seekcur_within_chunk_then_read();
int seekset_within_chunk_then_write_rw();
int seekcur_within_chunk_then_write_rw();
int seekset_within_chunk_then_read_rw();
int seekcur_within_chunk_then_read_rw();

int seekset_new_chunk_then_write();
int seekcur_new_chunk_then_write();
int seekset_new_chunk_then_read();
int seekcur_new_chunk_then_read();
int seekset_new_chunk_then_write_rw();
int seekcur_new_chunk_then_write_rw();
int seekset_new_chunk_then_read_rw();
int seekcur_new_chunk_then_read_rw();

int seekset_to_the_end_of_chunk_then_write();
int seekcur_to_the_end_of_chunk_then_write();
int seekset_to_the_end_of_chunk_then_read();
int seekcur_to_the_end_of_chunk_then_read();
int seekset_to_the_end_of_chunk_then_write_rw();
int seekcur_to_the_end_of_chunk_then_write_rw();
int seekset_to_the_end_of_chunk_then_read_rw();
int seekcur_to_the_end_of_chunk_then_read_rw();

int main(int argc, char *argv[]) {
    int num_tests = 56;
    int successful = 0;

    // read only/write only tests
    successful = successful + simple_write_test();
    successful = successful + simple_read_test();
    successful = successful + read_twice_from_file();
    successful = successful + write_twice_to_file();
    successful = successful + large_read_test();
    successful = successful + large_double_read_test();
    successful = successful + extra_large_read_test();
    successful = successful + large_write_test();
    successful = successful + extra_large_write_test();
    successful = successful + write_to_multiple_of_chunk_then_write();
    successful = successful + read_rest_of_chunk_then_read_again();
    //successful = successful + write_past_EOF();
    successful = successful + read_past_EOF();
    successful = successful + read_much_past_EOF();

    // read/write tests
    successful = successful + simple_write_test_rw();
    successful = successful + simple_read_test_rw();
    successful = successful + read_twice_from_file_rw();
    successful = successful + write_twice_to_file_rw();
    successful = successful + read_then_write_from_file();
    successful = successful + write_then_read_from_file();
    successful = successful + large_read_test_rw();
    successful = successful + large_double_read_test_rw();
    successful = successful + extra_large_read_test_rw();
    successful = successful + large_write_test_rw();
    successful = successful + extra_large_write_test_rw();
    successful = successful + large_read_then_write();
    successful = successful + extra_large_read_then_extra_large_write();
    successful = successful + large_write_then_read();
    successful = successful + extra_large_write_then_extra_large_read();
    successful = successful + write_to_multiple_of_chunk_then_write_rw();
    successful = successful + read_rest_of_chunk_then_read_again_rw();
    successful = successful + write_to_multiple_of_chunk_then_read();
    successful = successful + read_rest_of_chunk_then_write();

    // same chunk
    successful = successful + seekset_within_chunk_then_write();
    successful = successful + seekcur_within_chunk_then_write();
    successful = successful + seekset_within_chunk_then_read();
    successful = successful + seekcur_within_chunk_then_read();
    successful = successful + seekset_within_chunk_then_write_rw();
    successful = successful + seekcur_within_chunk_then_write_rw();
    successful = successful + seekset_within_chunk_then_read_rw();
    successful = successful + seekcur_within_chunk_then_read_rw();

    // different chunk
    successful = successful + seekset_new_chunk_then_write();
    successful = successful + seekcur_new_chunk_then_write();
    successful = successful + seekset_new_chunk_then_read();
    successful = successful + seekcur_new_chunk_then_read();
    successful = successful + seekset_new_chunk_then_write_rw();
    successful = successful + seekcur_new_chunk_then_write_rw();
    successful = successful + seekset_new_chunk_then_read_rw();
    successful = successful + seekcur_new_chunk_then_read_rw();

    // beginning of next chunk
    successful = successful + seekset_to_the_end_of_chunk_then_write();
    successful = successful + seekcur_to_the_end_of_chunk_then_write();
    successful = successful + seekset_to_the_end_of_chunk_then_read();
    successful = successful + seekcur_to_the_end_of_chunk_then_read();
    successful = successful + seekset_to_the_end_of_chunk_then_write_rw();
    successful = successful + seekcur_to_the_end_of_chunk_then_write_rw();
    successful = successful + seekset_to_the_end_of_chunk_then_read_rw();
    successful = successful + seekcur_to_the_end_of_chunk_then_read_rw();
    
    printf("%d failed tests out of %d\n", num_tests - successful, num_tests);
    
    return 0;
}

int compare_files(char *fname1, char *fname2) {
    FILE *fp1, *fp2;
    int ch1, ch2;

    fp1 = fopen(fname1, "r");
    fp2 = fopen(fname2, "r");

    if (fp1 == NULL)
    {
        printf("Cannot open %s for reading\n", fname1);
        //perror("open");
        exit(1);
    }
    else if (fp2 == NULL)
    {
        printf("Cannot open %s for reading\n", fname2);
        exit(1);
    }
    else
    {
        ch1 = getc(fp1);
        ch2 = getc(fp2);

        while ((ch1 != EOF) && (ch2 != EOF) && (ch1 == ch2))
        {
            ch1 = getc(fp1);
            ch2 = getc(fp2);
        }
        fclose(fp1);
        fclose(fp2);
        if (ch1 == ch2)
        {
            //printf("Files are identical\n");
            return 1;
        }
        else
        { //(ch1 != ch2)
            //printf("Files are Not identical\n");
            return 0;
        }
    }
}

/* 
About Test Nameing:
"rw" means it opens the file as RDWR but otherwise is the same as the test
    named the same but without the "rw"

seekset vs seekcur signifywhat type of seek is used
*/

// tests a single write
int simple_write_test(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJ";
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    file = myopen(file1, O_CREAT | O_TRUNC | O_WRONLY);
    mywrite(string, 12, file);
    myclose(file);

    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 12);
    close(fd);

    return compare_files(file1, file2);
}

// tests a single read
int simple_read_test(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJ";
    char string_read[13];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 12);
    close(fd);
    
    file = myopen(file1, O_RDONLY);
    myread(string_read, 12, file);
    myclose(file);
    
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string_read, 12);
    close(fd);

    return compare_files(file1, file2);
}

// reads twice from a file
int read_twice_from_file(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char string_read[30];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 20);
    close(fd);
    
    file = myopen(file1, O_RDONLY);
    myread(string_read, 10, file);
    myread(string_read + 10, 10, file);
    myclose(file);
    
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string_read, 20);
    close(fd);

    return compare_files(file1, file2);
}

// writes twice to a file
int write_twice_to_file(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 10);
    write(fd, string + 10, 10);
    close(fd);
    
    file = myopen(file2, O_CREAT | O_TRUNC | O_WRONLY);
    mywrite(string, 10, file);
    mywrite(string + 10, 10, file);
    myclose(file);

    return compare_files(file1, file2);
}

// reads more than 4 kibibytes from a file
int large_read_test(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char string_read[6000];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);
    
    file = myopen(file1, O_RDONLY);
    myread(string_read, 6000, file);
    myclose(file);
    
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
        write(fd, string_read, 6000);
    close(fd);

    return compare_files(file1, file2);
}

// reads more than 4 kibibytes from a file twice
int large_double_read_test(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char string_read[10000];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 500; i++){
        write(fd, string, 20);
    }
    close(fd);
    
    file = myopen(file1, O_RDONLY);
    myread(string_read, 5000, file);
    myread(string_read + 5000, 5000, file);
    myclose(file);
    
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
        write(fd, string_read, 10000);
    close(fd);

    return compare_files(file1, file2);
}

// reads more than 200 kibibytes from a file
int extra_large_read_test(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    //char string_read[10 * 20 * 10025];
    char *string_read = malloc(10 * 20 * 100025);
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 10 * 100025; i++){
        write(fd, string, 20);
    }
    close(fd);
    
    file = myopen(file1, O_RDONLY);
    myread(string_read, 10 * 20 * 100025, file);
    myclose(file);
    
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
        write(fd, string_read, 10 * 20 * 100025);
    close(fd);

    return compare_files(file1, file2);
}

// writes more than 4 kibibytes to a file
int large_write_test(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHI\nJKLMNOPQRSTUVWXYZ";
    char write_buf[6000];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    // setup large data
    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);

    // load data into write buffer
    fd = open(file1, O_RDONLY, 0666);
    read(fd, write_buf, 6000);
    close(fd);
    
    file = myopen(file2, O_CREAT | O_TRUNC | O_WRONLY);
    mywrite(write_buf, 6000, file);
    myclose(file);

    return compare_files(file1, file2);
}

// writes more than 200 kibibytes to a file
int extra_large_write_test(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHI\nJKLMNOPQRSTUVWXYZ";
    char write_buf[10 * 20 * 1025];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    // setup large data
    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 10 * 1025; i++){
        write(fd, string, 20);
    }
    close(fd);

    // load data into write buffer
    fd = open(file1, O_RDONLY, 0666);
    read(fd, write_buf, 10 * 20 * 1025);
    close(fd);
    
    file = myopen(file2, O_CREAT | O_TRUNC | O_WRONLY);
    mywrite(write_buf, 10 * 20 * 1025, file);
    myclose(file);

    return compare_files(file1, file2);
}

// writes exactly 4 kibibytes then writes again
int write_to_multiple_of_chunk_then_write(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHI\nJKLMNOPQRSTUVWXYZ";
    char write_buf[6000];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    // setup large data
    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);

    // load data into write buffer
    fd = open(file1, O_RDONLY, 0666);
    read(fd, write_buf, 6000);
    close(fd);
    
    file = myopen(file2, O_CREAT | O_TRUNC | O_WRONLY);
    mywrite(write_buf, 100, file);
    mywrite(write_buf + 100, 3996, file);
    mywrite(write_buf + 4096, 1904, file);
    myclose(file);

    return compare_files(file1, file2);
}

// reads exactly 4 kibibytes then reads some more
int read_rest_of_chunk_then_read_again(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char string_read[6000];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);
    
    file = myopen(file1, O_RDONLY);
    myread(string_read, 100, file);
    myread(string_read + 100, 3996, file);
    myread(string_read + 4096, 1904, file);
    myclose(file);
    
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
        write(fd, string_read, 6000);
    close(fd);

    return compare_files(file1, file2);
}

// writes past the end of the file size
int write_past_EOF(){
    return 0;
}
// read past the end of the file size
int read_past_EOF(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char string_read[6000];
    char *file1 = "./test.test";
    int amount_read = 0;

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 20);
    close(fd);
    
    file = myopen(file1, O_RDONLY);
    amount_read = myread(string_read, 100, file);
    myclose(file);
    if (amount_read == 20){
        return 1;
    } else {
        return 0;
    }
}
// read past the end of the file size by a lot
int read_much_past_EOF(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char string_read[6000];
    char *file1 = "./test.test";
    int amount_read = 0;

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 20);
    close(fd);
    
    file = myopen(file1, O_RDONLY);
    amount_read = myread(string_read, 10000, file);
    myclose(file);
    if (amount_read == 20){
        return 1;
    } else {
        return 0;
    }
}

// Read Write tests
int simple_write_test_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJ";
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    file = myopen(file1, O_CREAT | O_TRUNC | O_RDWR);
    mywrite(string, 12, file);
    myclose(file);

    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 12);
    close(fd);

    return compare_files(file1, file2);
}
int simple_read_test_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJ";
    char string_read[13];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 12);
    close(fd);
    
    file = myopen(file1, O_RDWR);
    myread(string_read, 12, file);
    myclose(file);
    
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string_read, 12);
    close(fd);

    return compare_files(file1, file2);
}
int read_twice_from_file_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char string_read[30];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 20);
    close(fd);
    
    file = myopen(file1, O_RDWR);
    myread(string_read, 10, file);
    myread(string_read + 10, 10, file);
    myclose(file);
    
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string_read, 20);
    close(fd);

    return compare_files(file1, file2);
}
int write_twice_to_file_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 10);
    write(fd, string + 10, 10);
    close(fd);
    
    file = myopen(file2, O_CREAT | O_TRUNC | O_RDWR);
    mywrite(string, 10, file);
    mywrite(string + 10, 10, file);
    myclose(file);

    return compare_files(file1, file2);
}

// single read then single write
int read_then_write_from_file(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char string_read[30];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 20);
    close(fd);
    
    file = myopen(file1, O_RDWR);
    myread(string_read, 10, file);
    mywrite(string + 6, 10, file);
    myclose(file);
    
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string_read, 10);
    write(fd, string + 6, 10);
    close(fd);

    return compare_files(file1, file2);
}

// single write then single read
int write_then_read_from_file(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char string_read[30];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 20);
    close(fd);
    
    file = myopen(file1, O_RDWR);
    mywrite(string + 6, 10, file);
    myread(string_read, 10, file);
    myclose(file);
    
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string + 6, 10);
    write(fd, string_read, 10);
    close(fd);

    return compare_files(file1, file2);
}

// more Read Write tests
int large_read_test_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char string_read[6000];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);
    
    file = myopen(file1, O_RDWR);
    myread(string_read, 6000, file);
    myclose(file);
    
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
        write(fd, string_read, 6000);
    close(fd);

    return compare_files(file1, file2);
}
int large_double_read_test_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char string_read[10000];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 500; i++){
        write(fd, string, 20);
    }
    close(fd);
    
    file = myopen(file1, O_RDWR);
    myread(string_read, 5000, file);
    myread(string_read + 5000, 5000, file);
    myclose(file);
    
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
        write(fd, string_read, 10000);
    close(fd);

    return compare_files(file1, file2);
}
int extra_large_read_test_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char string_read[10 * 20 * 1025];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 10 * 1025; i++){
        write(fd, string, 20);
    }
    close(fd);
    
    file = myopen(file1, O_RDWR);
    myread(string_read, 10 * 20 * 1025, file);
    myclose(file);
    
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
        write(fd, string_read, 10 * 20 * 1025);
    close(fd);

    return compare_files(file1, file2);
}
int large_write_test_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHI\nJKLMNOPQRSTUVWXYZ";
    char write_buf[6000];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    // setup large data
    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);

    // load data into write buffer
    fd = open(file1, O_RDONLY, 0666);
    read(fd, write_buf, 6000);
    close(fd);
    
    file = myopen(file2, O_CREAT | O_TRUNC | O_RDWR);
    mywrite(write_buf, 6000, file);
    myclose(file);

    return compare_files(file1, file2);
}
int extra_large_write_test_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHI\nJKLMNOPQRSTUVWXYZ";
    char write_buf[10 * 20 * 1025];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    // setup large data
    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 10 * 1025; i++){
        write(fd, string, 20);
    }
    close(fd);

    // load data into write buffer
    fd = open(file1, O_RDONLY, 0666);
    read(fd, write_buf, 10 * 20 * 1025);
    close(fd);
    
    file = myopen(file2, O_CREAT | O_TRUNC | O_RDWR);
    mywrite(write_buf, 10 * 20 * 1025, file);
    myclose(file);

    return compare_files(file1, file2);
}

// read more than 4 kibibytes then write a little bit
int large_read_then_write(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char string_read[6000];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);
    
    file = myopen(file1, O_RDWR);
    myread(string_read, 5990, file);
    mywrite(string + 2, 10, file);
    myclose(file);
    
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string_read, 5990);
    write(fd, string + 2, 10);
    close(fd);

    return compare_files(file1, file2);
}

// read more than 200 kibibytes then write more than 200 kibibytes
int extra_large_read_then_extra_large_write(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char string_read[2 * 10 * 20 * 1025];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 10 * 1025; i++){
        write(fd, string, 20);
    }
    for (int i = 0; i < 10 * 1025; i++){
        write(fd, string, 20);
    }
    close(fd);

    fd = open(file1, O_RDONLY, 0666);
    read(fd, string_read + 10 * 20 * 1025, 10 * 20 * 1025);
    close(fd);
    
    file = myopen(file1, O_RDWR);
    myread(string_read, 10 * 20 * 1025, file);
    mywrite(string_read + 10 * 20 * 1025, 10 * 20 * 1025, file);
    myclose(file);
    
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string_read, 10 * 20 * 1025);
    for (int i = 0; i < 10 * 1025; i++){
        write(fd, string, 20);
    }
    close(fd);

    return compare_files(file1, file2);
}

// write more than 4 kibibytes then read a little bit
int large_write_then_read(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char write_buf[6000];
    char read_buf[10];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    // setup large data
    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);

    // setup other large data
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string + 4, 20);
    }
    close(fd);

    // load data into write buffer
    fd = open(file1, O_RDONLY, 0666);
    read(fd, write_buf, 6000);
    close(fd);
    
    file = myopen(file2, O_RDWR);
    mywrite(write_buf, 5990, file);
    myread(read_buf, 10, file);
    myclose(file);

    fd = open(file1, O_WRONLY, 0666);
    lseek(fd, 5990, SEEK_CUR);
    write(fd, read_buf, 10);
    close(fd);


    return compare_files(file1, file2);
}

// write more than 200 kibibytes then read more than 200 kibibytes
int extra_large_write_then_extra_large_read(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char write_buf[10 * 20 * 1025];
    char read_buf[10 * 20 * 1025];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    // setup large data
    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 2 * 10 * 1025; i++){
        write(fd, string, 20);
    }
    close(fd);

    // setup other large data
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 2 * 10 * 1025; i++){
        write(fd, string + 4, 20);
    }
    close(fd);

    // load data into write buffer
    fd = open(file1, O_RDONLY, 0666);
    read(fd, write_buf, 10 * 20 * 1025);
    close(fd);
    
    file = myopen(file2, O_RDWR);
    mywrite(write_buf, 10 * 20 * 1025, file);
    myread(read_buf, 10 * 20 * 1025, file);
    myclose(file);

    fd = open(file1, O_WRONLY, 0666);
    lseek(fd, 10 * 20 * 1025, SEEK_CUR);
    write(fd, read_buf, 10 * 20 * 1025);
    close(fd);


    return compare_files(file1, file2);
}

// more Read Write tests
int write_to_multiple_of_chunk_then_write_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHI\nJKLMNOPQRSTUVWXYZ";
    char write_buf[6000];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    // setup large data
    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);

    // load data into write buffer
    fd = open(file1, O_RDONLY, 0666);
    read(fd, write_buf, 6000);
    close(fd);
    
    file = myopen(file2, O_CREAT | O_TRUNC | O_RDWR);
    mywrite(write_buf, 100, file);
    mywrite(write_buf + 100, 3996, file);
    mywrite(write_buf + 4096, 1904, file);
    myclose(file);

    return compare_files(file1, file2);
}
int read_rest_of_chunk_then_read_again_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char string_read[6000];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);
    
    file = myopen(file1, O_RDWR);
    myread(string_read, 100, file);
    myread(string_read + 100, 3996, file);
    myread(string_read + 4096, 1904, file);
    myclose(file);
    
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
        write(fd, string_read, 6000);
    close(fd);

    return compare_files(file1, file2);
}

// write exactly 4 kibibytes then read
int write_to_multiple_of_chunk_then_read(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char write_buf[6000];
    char read_buf[10];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    // setup large data
    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);

    // setup other large data
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string + 4, 20);
    }
    close(fd);

    // load data into write buffer
    fd = open(file1, O_RDONLY, 0666);
    read(fd, write_buf, 6000);
    close(fd);
    
    file = myopen(file2, O_RDWR);
    mywrite(write_buf, 4096, file);
    myread(read_buf, 1904, file);
    myclose(file);

    fd = open(file1, O_WRONLY, 0666);
    lseek(fd, 4096, SEEK_CUR);
    write(fd, read_buf, 1904);
    close(fd);


    return compare_files(file1, file2);
}

// read exactly 4 kibibytes then write
int read_rest_of_chunk_then_write(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char string_read[6000];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);
    
    file = myopen(file1, O_RDWR);
    myread(string_read, 4096, file);
    mywrite(string_read + 2, 1904, file);
    myclose(file);
    
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string_read, 4096);
    write(fd, string_read + 2, 1904);
    close(fd);

    return compare_files(file1, file2);
}

// seek inside current chunk then write
int seekset_within_chunk_then_write(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 20);
    lseek(fd, 10, SEEK_SET);
    write(fd, string + 2, 10);
    close(fd);
    
    file = myopen(file2, O_CREAT | O_TRUNC | O_WRONLY);
    mywrite(string, 20, file);
    myseek(file, 10, SEEK_SET);
    mywrite(string + 2, 10, file);
    myclose(file);

    return compare_files(file1, file2);
}
int seekcur_within_chunk_then_write(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 20);
    lseek(fd, -10, SEEK_CUR);
    write(fd, string + 2, 10);
    close(fd);
    
    file = myopen(file2, O_CREAT | O_TRUNC | O_WRONLY);
    mywrite(string, 20, file);
    myseek(file, -10, SEEK_CUR);
    mywrite(string + 2, 10, file);
    myclose(file);

    return compare_files(file1, file2);
}

// seek inside current chunk then read
int seekset_within_chunk_then_read(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *read_buf[10];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 20);
    close(fd);

    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 20);
    close(fd);
    
    file = myopen(file2, O_RDONLY);
    myread(read_buf, 10, file);
    myread(read_buf, 10, file);
    myseek(file, 5, SEEK_SET);
    myread(read_buf, 10, file);
    myclose(file);

    fd = open(file1, O_WRONLY, 0666);
    write(fd, string + 5, 10);
    close(fd);

    fd = open(file2, O_WRONLY, 0666);
    write(fd, read_buf, 10);
    close(fd);

    return compare_files(file1, file2);
}
int seekcur_within_chunk_then_read(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *read_buf[10];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 20);
    close(fd);

    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 20);
    close(fd);
    
    file = myopen(file2, O_RDONLY);
    myread(read_buf, 2, file);
    myseek(file, 3, SEEK_CUR);
    myread(read_buf, 10, file);
    myclose(file);

    fd = open(file1, O_WRONLY, 0666);
    write(fd, string + 5, 10);
    close(fd);

    fd = open(file2, O_WRONLY, 0666);
    write(fd, read_buf, 10);
    close(fd);

    return compare_files(file1, file2);
}

// Read Write tests
int seekset_within_chunk_then_write_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 20);
    lseek(fd, 10, SEEK_SET);
    write(fd, string + 2, 10);
    close(fd);
    
    file = myopen(file2, O_CREAT | O_TRUNC | O_RDWR);
    mywrite(string, 20, file);
    myseek(file, 10, SEEK_SET);
    mywrite(string + 2, 10, file);
    myclose(file);

    return compare_files(file1, file2);
}
int seekcur_within_chunk_then_write_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 20);
    lseek(fd, -10, SEEK_CUR);
    write(fd, string + 2, 10);
    close(fd);
    
    file = myopen(file2, O_CREAT | O_TRUNC | O_RDWR);
    mywrite(string, 20, file);
    myseek(file, -10, SEEK_CUR);
    mywrite(string + 2, 10, file);
    myclose(file);

    return compare_files(file1, file2);
}
int seekset_within_chunk_then_read_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *read_buf[10];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 20);
    close(fd);

    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 20);
    close(fd);
    
    file = myopen(file2, O_RDWR);
    myread(read_buf, 10, file);
    myread(read_buf, 10, file);
    myseek(file, 5, SEEK_SET);
    myread(read_buf, 10, file);
    myclose(file);

    fd = open(file1, O_WRONLY, 0666);
    write(fd, string + 5, 10);
    close(fd);

    fd = open(file2, O_WRONLY, 0666);
    write(fd, read_buf, 10);
    close(fd);

    return compare_files(file1, file2);
}
int seekcur_within_chunk_then_read_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *read_buf[10];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 20);
    close(fd);

    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    write(fd, string, 20);
    close(fd);
    
    file = myopen(file2, O_RDWR);
    myread(read_buf, 2, file);
    myseek(file, 3, SEEK_CUR);
    myread(read_buf, 10, file);
    myclose(file);

    fd = open(file1, O_WRONLY, 0666);
    write(fd, string + 5, 10);
    close(fd);

    fd = open(file2, O_WRONLY, 0666);
    write(fd, read_buf, 10);
    close(fd);

    return compare_files(file1, file2);
}

// seek to a different chunk then write
int seekset_new_chunk_then_write(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    lseek(fd, 10, SEEK_SET);
    write(fd, string + 2, 10);
    close(fd);
    
    file = myopen(file2, O_CREAT | O_TRUNC | O_WRONLY);
    for (int i = 0; i < 300; i++){
        mywrite(string, 20, file);
    }
    myseek(file, 10, SEEK_SET);
    mywrite(string + 2, 10, file);
    myclose(file);

    return compare_files(file1, file2);
}
int seekcur_new_chunk_then_write(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    lseek(fd, 10, SEEK_SET);
    write(fd, string + 2, 10);
    close(fd);
    
    file = myopen(file2, O_CREAT | O_TRUNC | O_WRONLY);
    for (int i = 0; i < 300; i++){
        mywrite(string, 20, file);
    }
    myseek(file, -5990, SEEK_CUR);
    mywrite(string + 2, 10, file);
    myclose(file);

    return compare_files(file1, file2);
}

// seek to a different chunk then read
int seekset_new_chunk_then_read(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *read_buf[10];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);    

    file = myopen(file2, O_RDONLY);
    myseek(file, 5000, SEEK_SET);
    myread(read_buf, 10, file);
    myclose(file);

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    lseek(fd, 5000, SEEK_SET);
    write(fd, read_buf, 10);
    close(fd);

    return compare_files(file1, file2);
}
int seekcur_new_chunk_then_read(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *read_buf[10];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);    

    file = myopen(file2, O_RDONLY);
    myseek(file, 5000, SEEK_CUR);
    myread(read_buf, 10, file);
    myclose(file);

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    lseek(fd, 5000, SEEK_SET);
    write(fd, read_buf, 10);
    close(fd);

    return compare_files(file1, file2);
}

// Read Write tests
int seekset_new_chunk_then_write_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    lseek(fd, 10, SEEK_SET);
    write(fd, string + 2, 10);
    close(fd);
    
    file = myopen(file2, O_CREAT | O_TRUNC | O_RDWR);
    for (int i = 0; i < 300; i++){
        mywrite(string, 20, file);
    }
    myseek(file, 10, SEEK_SET);
    mywrite(string + 2, 10, file);
    myclose(file);

    return compare_files(file1, file2);
}
int seekcur_new_chunk_then_write_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    lseek(fd, 10, SEEK_SET);
    write(fd, string + 2, 10);
    close(fd);
    
    file = myopen(file2, O_CREAT | O_TRUNC | O_RDWR);
    for (int i = 0; i < 300; i++){
        mywrite(string, 20, file);
    }
    myseek(file, -5990, SEEK_CUR);
    mywrite(string + 2, 10, file);
    myclose(file);

    return compare_files(file1, file2);
}
int seekset_new_chunk_then_read_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *read_buf[10];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);    

    file = myopen(file2, O_RDWR);
    myseek(file, 5000, SEEK_SET);
    myread(read_buf, 10, file);
    myclose(file);

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    lseek(fd, 5000, SEEK_SET);
    write(fd, read_buf, 10);
    close(fd);

    return compare_files(file1, file2);
}
int seekcur_new_chunk_then_read_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *read_buf[10];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);    

    file = myopen(file2, O_RDWR);
    myseek(file, 5000, SEEK_CUR);
    myread(read_buf, 10, file);
    myclose(file);

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    lseek(fd, 5000, SEEK_SET);
    write(fd, read_buf, 10);
    close(fd);

    return compare_files(file1, file2);
}

// seek to the beginning of the next chunk then write
int seekset_to_the_end_of_chunk_then_write(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);

    fd = open(file1, O_WRONLY, 0666);
    lseek(fd, 4096, SEEK_SET);
    write(fd, string, 10);
    close(fd);
    
    file = myopen(file2, O_WRONLY);
    myseek(file, 4096, SEEK_SET);
    mywrite(string, 10, file);
    myclose(file);

    return compare_files(file1, file2);
}
int seekcur_to_the_end_of_chunk_then_write(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);

    fd = open(file1, O_WRONLY, 0666);
    lseek(fd, 4096, SEEK_SET);
    write(fd, string, 10);
    close(fd);
    
    file = myopen(file2, O_WRONLY);
    myseek(file, 4096, SEEK_CUR);
    mywrite(string, 10, file);
    myclose(file);

    return compare_files(file1, file2);
}

// seek to the beginning of the next chunk then read
int seekset_to_the_end_of_chunk_then_read(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *read_buf[10];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);    

    file = myopen(file2, O_RDONLY);
    myseek(file, 4096, SEEK_SET);
    myread(read_buf, 10, file);
    myclose(file);

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    lseek(fd, 4096, SEEK_SET);
    write(fd, read_buf, 10);
    close(fd);

    return compare_files(file1, file2);
}
int seekcur_to_the_end_of_chunk_then_read(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *read_buf[10];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);    

    file = myopen(file2, O_RDONLY);
    myseek(file, 4096, SEEK_CUR);
    myread(read_buf, 10, file);
    myclose(file);

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    lseek(fd, 4096, SEEK_SET);
    write(fd, read_buf, 10);
    close(fd);

    return compare_files(file1, file2);
}

// Read Write tests
int seekset_to_the_end_of_chunk_then_write_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);

    fd = open(file1, O_WRONLY, 0666);
    lseek(fd, 4096, SEEK_SET);
    write(fd, string, 10);
    close(fd);
    
    file = myopen(file2, O_RDWR);
    myseek(file, 4096, SEEK_SET);
    mywrite(string, 10, file);
    myclose(file);

    return compare_files(file1, file2);
}
int seekcur_to_the_end_of_chunk_then_write_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);
    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);

    fd = open(file1, O_WRONLY, 0666);
    lseek(fd, 4096, SEEK_SET);
    write(fd, string, 10);
    close(fd);
    
    file = myopen(file2, O_WRONLY);
    myseek(file, 4096, SEEK_CUR);
    mywrite(string, 10, file);
    myclose(file);

    return compare_files(file1, file2);
}
int seekset_to_the_end_of_chunk_then_read_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *read_buf[10];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);    

    file = myopen(file2, O_RDWR);
    myseek(file, 4096, SEEK_SET);
    myread(read_buf, 10, file);
    myclose(file);

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    lseek(fd, 4096, SEEK_SET);
    write(fd, read_buf, 10);
    close(fd);

    return compare_files(file1, file2);
}
int seekcur_to_the_end_of_chunk_then_read_rw(){
    int fd;
    struct MYFILE *file;
    char *string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char *read_buf[10];
    char *file1 = "./test.test";
    char *file2 = "./test2.test";

    fd = open(file2, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    close(fd);    

    file = myopen(file2, O_RDWR);
    myseek(file, 4096, SEEK_CUR);
    myread(read_buf, 10, file);
    myclose(file);

    fd = open(file1, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    for (int i = 0; i < 300; i++){
        write(fd, string, 20);
    }
    lseek(fd, 4096, SEEK_SET);
    write(fd, read_buf, 10);
    close(fd);

    return compare_files(file1, file2);
}
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "myio.h"

static const size_t CHUNK_SIZE = 4096;

off_t get_size(int file_descriptor);


off_t get_size(int file_descriptor){
    struct stat statbuf;
    if (fstat(file_descriptor, &statbuf) == -1){
        return -1; 
    }
    return statbuf.st_size;
}

struct MYFILE *myopen(char *pathname, int flags){
    struct MYFILE *file = malloc(sizeof(struct MYFILE));
    if (file == NULL){
        return NULL;
    }

    file->open_flags = flags;

    if((file->file_descriptor = open(pathname, flags, 0666)) == -1){
        // clean up
        free(file);
        return NULL;
    }
    
    file->end_of_data = get_size(file->file_descriptor);

    off_t map_size = file->end_of_data + (CHUNK_SIZE - (file->end_of_data % CHUNK_SIZE));

    if (O_RDWR == (flags & O_RDWR)){
        if (ftruncate(file->file_descriptor, map_size) == -1){
            // clean up
            free(file);
            return NULL;
        }
        file->buf = mmap(NULL, map_size, PROT_WRITE | PROT_READ, MAP_SHARED, file->file_descriptor, 0);
    }
    else if (O_WRONLY == (flags & O_WRONLY)){
        if(close(file->file_descriptor) == -1){
            // clean up
            free(file);
            return NULL;
        }
        if((file->file_descriptor = open(pathname, O_RDWR, 0666)) == -1){
            // clean up
            free(file);
            return NULL;
        }
        if (ftruncate(file->file_descriptor, map_size) == -1){
            // clean up
            free(file);
            return NULL;
        }
        file->buf = mmap(NULL, map_size, PROT_WRITE, MAP_SHARED, file->file_descriptor, 0);
    }
    else if (O_RDONLY == (flags & O_RDONLY)){
        file->buf = mmap(NULL, file->end_of_data, PROT_READ, MAP_PRIVATE, file->file_descriptor, 0);
    }

    if(file->buf == MAP_FAILED) {
        // clean up
        free(file);
        return NULL;
    }
    
    file->user_offset = 0;

    return file;
}

int myclose(struct MYFILE *file){
    if (munmap(file->buf, get_size(file->file_descriptor)) == -1){
        return EOF;
    }
    if (O_WRONLY == (file->open_flags & O_WRONLY) || O_RDWR == (file->open_flags & O_RDWR)){
        if (ftruncate(file->file_descriptor, file->end_of_data) == -1){
            return EOF;
        }
    }
    if(close(file->file_descriptor) == -1){
        return EOF;
    }
    free(file);

    return 0;
}

size_t myread(void *ptr, size_t size, struct MYFILE *file){
    // adjust if reading past end of file
    if (file->end_of_data - file->user_offset < size){
        size = file->end_of_data - file->user_offset;
    }
    memcpy(ptr, (char *)file->buf + file->user_offset, size);
    file->user_offset = file->user_offset + size;
    return size;
}

size_t mywrite(void *ptr, size_t size, struct MYFILE *file){
    
    off_t file_size = get_size(file->file_descriptor);
    off_t free_space = file_size - file->user_offset;
    
    if (size > free_space){
        off_t missing_space = size - free_space;
        off_t map_size = file_size + missing_space + (CHUNK_SIZE - ((file_size + missing_space) % CHUNK_SIZE));
        if (munmap(file->buf, file_size) == -1){
            return -1;
        }
        if (ftruncate(file->file_descriptor, map_size) == -1){
            return -1;
        }

        if (O_WRONLY == (file->open_flags & O_WRONLY) || O_RDWR == (file->open_flags & O_RDWR)){
            file->buf = mmap(NULL, map_size, PROT_WRITE, MAP_SHARED, file->file_descriptor, 0);
        } else {
            // error open as read only
            return -1;
        }

        if(file->buf == MAP_FAILED) {
            return -1;
        }
    }
    memcpy((char *)file->buf + file->user_offset, ptr, size);
    file->user_offset = file->user_offset + size;
    if (file->user_offset > file->end_of_data){
        file->end_of_data = file->user_offset;
    }
    return size;
}

int myflush(struct MYFILE *file){
    if (msync(file->buf, get_size(file->file_descriptor), MS_SYNC) == -1){
        return EOF;
    }
    return 0;
}

off_t myseek(struct MYFILE *file, off_t offset, int whence){
    // change offset to absolute offset if relative is given
    if (whence == SEEK_CUR){
        offset = file->user_offset + offset;
    }
    off_t file_size = get_size(file->file_descriptor);
    if (offset > file_size){
        off_t map_size = offset + (CHUNK_SIZE - (offset % CHUNK_SIZE));
        if (munmap(file->buf, file_size) == -1){
            return -1;
        }
        if (ftruncate(file->file_descriptor, map_size) == -1){
            return -1;
        }
        if (O_WRONLY == (file->open_flags & O_WRONLY) || O_RDWR == (file->open_flags & O_RDWR)){
            file->buf = mmap(NULL, map_size, PROT_WRITE, MAP_SHARED, file->file_descriptor, 0);
        } else {
            // error open as read only
            return -1;
        }
        if(file->buf == MAP_FAILED) {
            return -1;
        }
    }
    file->user_offset = offset;
    return offset;
}
CFLAGS=-g -Wall -pedantic -fPIC

test-myio: libmymmapio.so test-myio.o myio.h
	gcc -L. -lmymmapio -o $@ test-myio.o

libmymmapio.so: mymmapio.o
	gcc -shared -Wl,-soname,$@ -o $@ $^

%.o: %.c
	gcc $(CFLAGS) -c -o $@ $^

.PHONY: clean
clean:
	rm -f test-myio *.o *.test *.so